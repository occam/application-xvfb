# Install Dependencies
apt-get -y install libpixman-1-dev libx11-dev libgl1-mesa-dev libxfont-dev libgcrypt11-dev libxkbfile-dev
apt-get -y install x11proto-scrnsaver-dev x11proto-record-dev x11proto-composite-dev x11proto-video-dev
apt-get -y install x11proto-fonts-dev x11proto-xinerama-dev x11proto-render-dev x11proto-bigreqs-dev
apt-get -y install x11proto-xcmisc-dev x11proto-resource-dev x11proto-xf86dri-dev x11proto-randr-dev
apt-get -y install libpciaccess-dev

# xkeyboard-config needs:
apt-get -y install intltool

# Unpack xkbcomp
tar -xvf xkbcomp*.gz

# Build/Install xkbcomp
cd xkbcomp*
./configure
make
make install
cd ..

# Unpack xkeyboard-config
tar -xvf xkeyboard*.gz

# Build/Install xkeyboard-config
cd xkeyboard*
./configure
make
make install
cd ..

# Unpack xorg
tar -xvf xorg-server*.gz

# Build/Install xorg-server
cd xorg*
./configure
make
make install
cd ..

# Copy dependent libraries
mkdir lib
cp /lib/x86_64-linux-gnu/* lib/.
cp /usr/lib/x86_64-linux-gnu/libSDL* lib/.
cp /usr/lib/x86_64-linux-gnu/mesa/* lib/.
cp /usr/lib/x86_64-linux-gnu/xorg/* lib/.
cp /usr/lib/x86_64-linux-gnu/pulseaudio/* lib/.
cp /usr/lib/x86_64-linux-gnu/* lib/.

echo Done
