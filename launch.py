import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
binary         = "%s/xorg-server-1.12.2/hw/vfb/Xvfb" % (scripts_path)
lib_path       = "%s/lib" % (scripts_path)
job_path       = os.getcwd()

object = Occam.load()

# Form arguments
args = [binary,
        '-ac',
        '-screen',
        '0',
        '1024x768x16']

# Form command line
command = ' '.join(args)

# Tell OCCAM how to run
Occam.report(command, env={"LD_LIBRARY_PATH": lib_path})
